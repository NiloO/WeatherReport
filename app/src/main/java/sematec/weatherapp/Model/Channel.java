
package sematec.weatherapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel {

    @SerializedName("units")
    @Expose
    private sematec.weatherapp.Units units;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("lastBuildDate")
    @Expose
    private String lastBuildDate;
    @SerializedName("ttl")
    @Expose
    private String ttl;
    @SerializedName("location")
    @Expose
    private sematec.weatherapp.Location location;
    @SerializedName("wind")
    @Expose
    private sematec.weatherapp.Wind wind;
    @SerializedName("atmosphere")
    @Expose
    private sematec.weatherapp.Atmosphere atmosphere;
    @SerializedName("astronomy")
    @Expose
    private sematec.weatherapp.Astronomy astronomy;
    @SerializedName("image")
    @Expose
    private sematec.weatherapp.Image image;
    @SerializedName("item")
    @Expose
    private sematec.weatherapp.Item item;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Channel() {
    }

    /**
     * 
     * @param wind
     * @param location
     * @param link
     * @param atmosphere
     * @param image
     * @param ttl
     * @param astronomy
     * @param units
     * @param title
     * @param description
     * @param item
     * @param lastBuildDate
     * @param language
     */
    public Channel(sematec.weatherapp.Units units, String title, String link, String description, String language, String lastBuildDate, String ttl, sematec.weatherapp.Location location, sematec.weatherapp.Wind wind, sematec.weatherapp.Atmosphere atmosphere, sematec.weatherapp.Astronomy astronomy, sematec.weatherapp.Image image, sematec.weatherapp.Item item) {
        super();
        this.units = units;
        this.title = title;
        this.link = link;
        this.description = description;
        this.language = language;
        this.lastBuildDate = lastBuildDate;
        this.ttl = ttl;
        this.location = location;
        this.wind = wind;
        this.atmosphere = atmosphere;
        this.astronomy = astronomy;
        this.image = image;
        this.item = item;
    }

    public sematec.weatherapp.Units getUnits() {
        return units;
    }

    public void setUnits(sematec.weatherapp.Units units) {
        this.units = units;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public sematec.weatherapp.Location getLocation() {
        return location;
    }

    public void setLocation(sematec.weatherapp.Location location) {
        this.location = location;
    }

    public sematec.weatherapp.Wind getWind() {
        return wind;
    }

    public void setWind(sematec.weatherapp.Wind wind) {
        this.wind = wind;
    }

    public sematec.weatherapp.Atmosphere getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(sematec.weatherapp.Atmosphere atmosphere) {
        this.atmosphere = atmosphere;
    }

    public sematec.weatherapp.Astronomy getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(sematec.weatherapp.Astronomy astronomy) {
        this.astronomy = astronomy;
    }

    public sematec.weatherapp.Image getImage() {
        return image;
    }

    public void setImage(sematec.weatherapp.Image image) {
        this.image = image;
    }

    public sematec.weatherapp.Item getItem() {
        return item;
    }

    public void setItem(sematec.weatherapp.Item item) {
        this.item = item;
    }

}
