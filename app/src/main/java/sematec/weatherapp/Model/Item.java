
package sematec.weatherapp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("pubDate")
    @Expose
    private String pubDate;
    @SerializedName("condition")
    @Expose
    private sematec.weatherapp.Condition condition;
    @SerializedName("forecast")
    @Expose
    private List<sematec.weatherapp.Forecast> forecast = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("guid")
    @Expose
    private sematec.weatherapp.Guid guid;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Item() {
    }

    /**
     * 
     * @param guid
     * @param pubDate
     * @param title
     * @param _long
     * @param forecast
     * @param condition
     * @param description
     * @param link
     * @param lat
     */
    public Item(String title, String lat, String _long, String link, String pubDate, sematec.weatherapp.Condition condition, List<sematec.weatherapp.Forecast> forecast, String description, sematec.weatherapp.Guid guid) {
        super();
        this.title = title;
        this.lat = lat;
        this._long = _long;
        this.link = link;
        this.pubDate = pubDate;
        this.condition = condition;
        this.forecast = forecast;
        this.description = description;
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public sematec.weatherapp.Condition getCondition() {
        return condition;
    }

    public void setCondition(sematec.weatherapp.Condition condition) {
        this.condition = condition;
    }

    public List<sematec.weatherapp.Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(List<sematec.weatherapp.Forecast> forecast) {
        this.forecast = forecast;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public sematec.weatherapp.Guid getGuid() {
        return guid;
    }

    public void setGuid(sematec.weatherapp.Guid guid) {
        this.guid = guid;
    }

}
