package sematec.weatherapp;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import android.graphics.Typeface;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView code, low,high,currenttemp;
    EditText cityName;
    //TextView weatherIcon;
   // Typeface weatherFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // sunSet = (TextView) findViewById(R.id.sunSet);

        findViewById(R.id.showResult).setOnClickListener(this);
        code = (EditText) findViewById(R.id.code);
        cityName = (EditText) findViewById(R.id.cityName);
        low = (TextView)findViewById(R.id.low);
        high = (TextView)findViewById(R.id.high);
        currenttemp = (TextView)findViewById(R.id.currenttemp);

    }

    @Override
    public void onClick(View view) {
        getDataByAsync();
      //  weatherIcon = (TextView) findViewById(R.id.weather_icon);

       // weatherIcon.setTypeface(weatherFont);

    }

    public void getDataByAsync() {


        String city = cityName.getText().toString();
        String query = "select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%22)&format=json";
        String baseurl = "https://query.yahooapis.com/v1/public/yql?q=" + query;


        String GET_URL = baseurl;
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(GET_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, "error : " + throwable, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndSetData(responseString);
            }
        });

    }

    void parseAndSetData(String jsonSTR) {
        Gson gson = new Gson();
        sematec.weatherapp.WeatherModel model = gson.fromJson(jsonSTR, sematec.weatherapp.WeatherModel.class);
       // String value = model.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDate();
        code.setText(model.getQuery().getResults().getChannel().getItem().getForecast().get(0).getText());


        model.getQuery().getResults().getChannel().getImage();
        model.getQuery().getResults().getChannel().getItem();


        currenttemp.setText(    model.getQuery().getResults().getChannel().getItem().getCondition().getTemp());

        Typeface weatherTypeFace = Typeface.createFromAsset(this.getAssets(), "fonts/weathericons-regular-webfont.ttf");
        TextView tvWeatherStatus = (TextView) findViewById(R.id.tvWeatherStatus);
        tvWeatherStatus.setText("\uF086");
        tvWeatherStatus.setTypeface(weatherTypeFace);


    }

    private void setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = this.getString(R.string.weather_sunny);
            } else {
                icon = this.getString(R.string.weather_clear_night);
            }
        } else {
            switch (id) {
                case 2:
                    icon = this.getString(R.string.weather_thunder);
                    break;
                case 3:
                    icon = this.getString(R.string.weather_drizzle);
                    break;
                case 7:
                    icon = this.getString(R.string.weather_foggy);
                    break;
                case 8:
                    icon = this.getString(R.string.weather_cloudy);
                    break;
                case 6:
                    icon = this.getString(R.string.weather_snowy);
                    break;
                case 5:
                    icon = this.getString(R.string.weather_rainy);
                    break;
            }
        }
       // weatherIcon.setText(icon);
    }

    private double ConvertToCelsius(double fahrenheit) {
        double celsius=((fahrenheit - 32) * 5 /9 );
        return celsius;
    }
}
